package com.example.troubleshoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TroubleshootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TroubleshootApplication.class, args);
	}

}
