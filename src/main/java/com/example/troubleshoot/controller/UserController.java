package com.example.troubleshoot.controller;

import com.example.troubleshoot.TroubleshootApplication;
import com.example.troubleshoot.model.User;
import com.example.troubleshoot.service.UserService;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    private UserService service = new UserService();

    Logger logger = LoggerFactory.getLogger(TroubleshootApplication.class);

    @GetMapping("/getUser/{id}")
    public User getUserById(@PathVariable int id) {
        return service.getUserById(id);
    }

    @GetMapping("/getUser")
    public List<User> getUsers() {
        val users = service.getUsers();
        logger.info("number of users: {}", users.size());
        return users;
    }
}
