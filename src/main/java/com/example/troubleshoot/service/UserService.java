package com.example.troubleshoot.service;

import com.example.troubleshoot.TroubleshootApplication;
import com.example.troubleshoot.model.User;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {

    Logger logger = LoggerFactory.getLogger(TroubleshootApplication.class);

    public User getUserById(int id) {
        val users = getUsers();
        val user = users.stream().filter(u -> u.getId()==id).findAny().orElse(null);
        if (user != null) {
            logger.info("user found: {}", user);
            return user;
        } else {
            try{
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("user not found with id: {}", id);
            }
            return new User();
        }
    }

    public List<User> getUsers() {
        return Stream.of(new User(1, "John"),
                        new User(2, "Shyam"),
                        new User(3, "Rony"),
                        new User(4, "mak"))
                .collect(Collectors.toList());
    }
}
